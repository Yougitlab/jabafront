import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyServiceService } from './service/my-service.service';
import { AcceuilComponent } from './acceuil/acceuil/acceuil.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { DetailProduitComponent } from './detail-produit/detail-produit.component';
import { CaddyComponent } from './caddy/caddy.component';
import { AddUserComponent } from './add-user/add-user.component';

import { CategorieComponent } from './categorie/categorie.component';
import { Admine1Module } from './admine/admine1/admine1.module';
import { ProduitParCategorieComponent } from './acceuil/produit-par-categorie/produit-par-categorie.component';

@NgModule({
  declarations: [
    AppComponent,
    AcceuilComponent,
    LoginComponent,
    DetailProduitComponent,
    CaddyComponent,
    AddUserComponent,
  
    CategorieComponent,
    ProduitParCategorieComponent,

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Admine1Module,
  ],
  providers: [MyServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
