import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AcceuilComponent } from './acceuil/acceuil/acceuil.component';
import { LoginComponent } from './login/login.component';
import { DetailProduitComponent } from './detail-produit/detail-produit.component';
import { CaddyComponent } from './caddy/caddy.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AddCategorieComponent } from './admine/add-categorie/add-categorie.component';
import { Admine1Component } from './admine/admine1/admine1.component';
import { OrdersGComponent } from './admine/orders-g/orders-g.component';
import { RapportComponent } from './admine/rapport/rapport.component';
import { AddProductComponent } from './admine/add-product/add-product.component';
import { UsersComponent } from './admine/users/users.component';
import { ProduitParCategorieComponent } from './acceuil/produit-par-categorie/produit-par-categorie.component';
import { AuthGardService } from './service/auth-gard.service';




const routes: Routes = [
  {path:"acceuil",component:AcceuilComponent},
  {path:"categorie/:id",component:ProduitParCategorieComponent},
  
  {path:"login",component:LoginComponent},
  {path:"detailProduit/:id",component:DetailProduitComponent},
  {path:"caddy",component:CaddyComponent},
  {path:"addUser",component:AddUserComponent},
  
 
  {path:"admine",canActivate:[AuthGardService],component:Admine1Component,
  children:[
    {path:"order",component:OrdersGComponent},
    {path:"",component:RapportComponent},
    {path:"product",component:AddProductComponent},
    {path:"users",component:UsersComponent},
    {path:"addCategorie",component:AddCategorieComponent},
    
  ]},
  {path:'**', redirectTo:'/acceuil',pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
