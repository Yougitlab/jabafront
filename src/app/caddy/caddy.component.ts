import { Component, OnInit } from '@angular/core';
import { CaddiesService } from '../service/caddies.service';
import { OrderService } from '../service/order.service';
import { MyServiceService } from '../service/my-service.service';
import { Order } from '../models/Order-model';
import { Router } from '@angular/router';
import { logging } from 'protractor';
import { LogInService } from '../service/log-in.service';

@Component({
  selector: 'app-caddy',
  templateUrl: './caddy.component.html',
  styleUrls: ['./caddy.component.css']
})
export class CaddyComponent implements OnInit {

  private order:Order;
  constructor(private route:Router, private servCaddy:CaddiesService,private orderServ:OrderService,private myServ:MyServiceService,private logServ:LogInService) { }
  addClient(){
    if(this.logServ.isAuthenticate && this.logServ.currenteUser.profile=="Client"){
      
      console.log(this.logServ.currenteUser)
      this.orderServ.loadProductItem();
    this.orderServ.curentOrder.utilisateur=this.logServ.currenteUser;
    
    //client deja inscrit
    this.myServ.postOrder("/commander",this.orderServ.curentOrder).subscribe(
      (data)=>{
        this.order=data;
      },
      (err)=>{
        console.log(err);
      }
    )
    }else{
      this.route.navigate(["/addUser"])
    }
    

  }
  saveOrder(){
    this.orderServ.loadProductItem();
    console.log(this.orderServ.curentOrder.products)
    console.log(this.orderServ.curentOrder.utilisateur)
    console.log(this.orderServ.curentOrder.totalOrder)
    this.myServ.postOrder("/myOrder",this.orderServ.curentOrder).subscribe(
      (data)=>{
        this.order=data;
      },
      (err)=>{
        console.log(err);
      }
    )
  }

  ngOnInit() {
  }

}
