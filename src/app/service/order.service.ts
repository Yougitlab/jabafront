import { Injectable } from '@angular/core';
import { Order } from '../models/Order-model';
import { CaddiesService } from './caddies.service';
import { ProductItem } from '../models/Product-item';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  public curentOrder:Order=new Order();
  constructor(private caddyServ:CaddiesService) { }
  loadProductItem(){

    this.curentOrder.products=[];
    this.curentOrder.totalOrder=0;
    this.caddyServ.getCaddy().forEach((p)=>{
      this.curentOrder.products.push(p);
      
      this.curentOrder.totalOrder+=p.price;
    })
      
    
      
    
  }
}
