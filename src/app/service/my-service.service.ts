import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product-model';
import { Order } from '../models/Order-model';

@Injectable({
  providedIn: 'root'
})
export class MyServiceService {
  private host='http://localhost:3020';
  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    }),
};
  constructor(private http:HttpClient) { }
  getResource(url){

    return this.http.get(this.host+url);
  }
  getProduct(url):Observable<Product>{
    return this.http.get<Product>(this.host+url);
  }
  postOrder(url,order:Order):Observable<Order>{
    
    return this.http.post<Order>(this.host+url,order,this.httpOptions);
  }
  uploadPhoto(file:File,url:string):Observable<HttpEvent<{}>>{
    let formData:FormData =new FormData();
    formData.append("file",file)
    const req=new HttpRequest("POST",this.host+url,formData)
    return this.http.request(req)
  }
  postData(url,data){
      return this.http.post(this.host+url,data,this.httpOptions);
  }
  deleteData(url){
    return this.http.delete(this.host+url,this.httpOptions);
  }
  
}
