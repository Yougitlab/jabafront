import { Injectable } from '@angular/core';
import { MyServiceService } from './my-service.service';

@Injectable({
  providedIn: 'root'
})
export class LogInService {
  private users=[
    {username:"Ndiaga",password:"1234",roles:['Adm','User']},
    {username:"You",password:"123",roles:['User']},
    {username:"Mouha",password:"12345",roles:['Adm']},
  ]

  public isAuthenticate;
  public currenteUser;
  public token:string; 
  public admine=true;
  constructor(private service:MyServiceService) { }
  logOut(){
    localStorage.removeItem('user');
    this.isAuthenticate=false;
    this.currenteUser=null;
  }
  login(username:string,password:string){
    this.service.getResource("/utilisateurs/search/login?tel="+username+"&&pswd="+password).subscribe(
      (data)=>{
      this.currenteUser=data;
      this.isAuthenticate=true
      this.token=btoa(JSON.stringify({id:this.currenteUser.id,nom: this.currenteUser.nom,prenom: this.currenteUser.prenom,tel: this.currenteUser.tel,profile:this.currenteUser.profile}))
      localStorage.setItem('user',this.token);
     
    },
    (err)=>{
      console.log(err);
    })
    // this.users.forEach(u=>{
    //   if(u.username==username && u.password==password){

    //     this.isAuthenticate=true
    //     this.currenteUser=u
    //     this.token=btoa(JSON.stringify({username:u.username,roles:u.roles}))
    //     localStorage.setItem('user',this.token);
    //   }
    // })
  }
  loadLocalStorage(){
    let rr=null;
    rr=atob(localStorage.getItem('user'));
    console.log(rr)
    if(rr!='ée'){
      this.currenteUser=JSON.parse(rr);
    if(this.currenteUser){
      console.log(this.currenteUser);
      this.isAuthenticate=true;
    }
    }
    
    
  }
}
