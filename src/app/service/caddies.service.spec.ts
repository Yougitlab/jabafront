import { TestBed } from '@angular/core/testing';

import { CaddiesService } from './caddies.service';

describe('CaddiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CaddiesService = TestBed.get(CaddiesService);
    expect(service).toBeTruthy();
  });
});
