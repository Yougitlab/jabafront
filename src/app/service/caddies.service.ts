import { Injectable } from '@angular/core';
import { Caddy } from '../models/caddy-model';
import { ProductItem } from '../models/Product-item';
import { Product } from '../models/product-model';
import { LogInService } from './log-in.service';

@Injectable({
  providedIn: 'root'
})
export class CaddiesService {
  public currentCaddie:Caddy;
  public caddy:Caddy;
  constructor() {
    let cc:string=localStorage.getItem("curentCaddy");
    if(cc){
      console.log(cc);
      let myCaddie:Caddy=JSON.parse(cc)
      this.currentCaddie=myCaddie;
      this.caddy=myCaddie;

    }else{
      this.caddy=new Caddy("MonPanier");
    }
   }
  public addProductToCaddy(product:Product,quantity:number){
    
    let curentProductitem=this.caddy.items[product.id.toString()];
    if(curentProductitem){
      this.caddy.items[product.id.toString()].quantite+=quantity;
    }else{
      let p=new ProductItem();
      p.id=product.id
      p.product=product;
      p.quantite=quantity;
      p.designation=product.designation;
      let a=product.currentPrice*quantity
      p.price=a
      console.log("nnnnoooooooooooooooooooooooooooooooo"+p.id)
      this.caddy.items[p.id.toString()]=p;
      
    }
    
    this.currentCaddie=this.caddy
    this.saveCaddy()
    
  }
  public getCaddy():ProductItem []{
    let a:ProductItem []=[];
    let i=0;
    for(let key in this.currentCaddie.items){
      a[i]=this.caddy.items[key];
      i++;
    }
    return a;

  }
 public loadCaddy(){
  
    let cc=JSON.parse(localStorage.getItem('curentCaddy'));
    if(cc){
     
      this.caddy=cc

    }else{
      this.caddy=new Caddy("MonPanier");
    }
   
 }
 getTotalPriceCaddy():number{
   let total=0;
  for (let k in this.currentCaddie.items) {
    total+=(this.currentCaddie.items[k].price*this.currentCaddie.items[k].quantite);
  }
  return total;
 }
 saveCaddy(){
  localStorage.setItem('curentCaddy',JSON.stringify(this.currentCaddie))
 }
}
