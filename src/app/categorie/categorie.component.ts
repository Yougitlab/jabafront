import { Component, OnInit } from '@angular/core';
import { MyServiceService } from '../service/my-service.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent implements OnInit {

  constructor(private service:MyServiceService) { }
  categories;
  ngOnInit() {
    this.service.getResource("/categories").subscribe(
      (data)=>{
        this.categories=data;
        console.log(data);
        console.log("bbbb");
      },
      (err)=>{
        console.log(err)
        console.log("ffff");

      },
    )
  }

}
