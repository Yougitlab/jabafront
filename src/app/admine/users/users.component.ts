import { Component, OnInit } from '@angular/core';
import { MyServiceService } from 'src/app/service/my-service.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private myServ:MyServiceService,private formB:FormBuilder) { }
  users
  page=0;
  totalPages=0;
  userForm;
  isUpdate=false;
  add=false
  password;
  profile;
  nextPage(){
    this.page++;
   this.getUsers();
    console.log(this.totalPages)
  }
  previousPage(){
    this.page--;
    this.getUsers();
  }
  getUsers(){
    this.myServ.getResource("/utilisateurs?page="+this.page+"&&size=8&&sort=date").subscribe(
      (data)=>{
        this.users=data
        console.log(data);
        this.totalPages=data['page']['totalPages']
        
      },
      (err)=>{
        console.log(err);
      }    
  );
  }
  MustMatch(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        console.log(control)
        console.log(matchingControl)

        

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}
  get f(){return this.userForm.controls}

  ngOnInit() {
    this.userForm=this.formB.group(
      {
      id:"",
      nom:['',Validators.required],
      prenom:['',Validators.required],
      tel:['',Validators.required],
      profile:['',Validators.required],
      password:['',Validators.required],
      confPassword:['',Validators.required],

    },
    {validator: this.MustMatch('password', 'confPassword')}
    )

  
  
    this.getUsers();
  }
  addP(){
    this.add=true
  }
  addUser(){
    this.add=true;
    if(!this.isUpdate){
      this.profile=this.userForm.value['profile'];
      this.password=this.userForm.value['password'];
    }
    let user={id:this.userForm.value['id'],nom:this.userForm.value['nom'],prenom:this.userForm.value['prenom'],tel:this.userForm.value['tel'],pswd:this.password,profile:this.profile}
    //console.log(user);
    this.myServ.postData("/addUser",user).subscribe(
      (data)=>{
        console.log(data);
        this.add=false
        this.getUsers()
      },
      (err)=>{
        console.log(err);
      }
    )
  }
  update(id){
    this.add=true
    this.isUpdate=true;
    this.myServ.getResource("/utilisateurs/"+id).subscribe(
      (data)=>{
        this.profile=data['profile'];
        this.password=data['pswd'];
        this.userForm.patchValue(
          {
          id:data['id'],
          nom:data['nom'],
          prenom:data['prenom'],
          tel:data['tel'],
          profile:data['profile']
        }
       
          )
          console.log(this.userForm)
      },
      (err)=>{

      }
    )
    

  }
  saveUpdate(){

  }

}
