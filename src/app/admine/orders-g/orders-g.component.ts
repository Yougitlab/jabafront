import { Component, OnInit } from '@angular/core';
import { MyServiceService } from 'src/app/service/my-service.service';

@Component({
  selector: 'app-orders-g',
  templateUrl: './orders-g.component.html',
  styleUrls: ['./orders-g.component.css']
})
export class OrdersGComponent implements OnInit {

  constructor(private service: MyServiceService) { }

  orders;
  productItems;
  orderV=true
      getPductItem(id){
        this.orderV=false
        this.service.getResource("/commandes/"+id+"/products").subscribe(
          (data)=>{
              this.productItems=data
              console.log(data)
          },
          (err)=>{
            console.log(err)
          }
        )
      }
      ngOnInit() {
        
    this.service.getResource("/commandes").subscribe(
      (data)=>{
          this.orders=data
          console.log(data)
      },
      (err)=>{
        console.log(err)
      }
    )
  }

}
