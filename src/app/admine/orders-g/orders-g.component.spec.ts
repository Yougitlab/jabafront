import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersGComponent } from './orders-g.component';

describe('OrdersGComponent', () => {
  let component: OrdersGComponent;
  let fixture: ComponentFixture<OrdersGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
