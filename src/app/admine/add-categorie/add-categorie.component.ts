import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MyServiceService } from 'src/app/service/my-service.service';

@Component({
  selector: 'app-add-categorie',
  templateUrl: './add-categorie.component.html',
  styleUrls: ['./add-categorie.component.css']
})
export class AddCategorieComponent implements OnInit {

  constructor(private formB:FormBuilder,private myServ:MyServiceService) { }
  categorieForm;
  order;
  categories;
  page=0;
  totalPages=0;
  add=false;
  isUpdate;
  ngOnInit() {
    this.getCategories();
    this.categorieForm=this.formB.group(
      {
        id:[''],
      designation:['',Validators.required],
      description:['',Validators.required],
    }
    )

  }
 
  get f(){return this.categorieForm.controls}
  nextPage(){
    
    if(this.page<this.totalPages){
      this.page++;
      this.getCategories();
      console.log(this.page)
    }
    
  }
  previousPage(){
    if(this.page>0){
      this.page--;
      this.getCategories();
    }
    
  }
  getPage(i){
    this.page=i;
      this.getCategories();
  }
  addP(){
    this.add=true
  }
  update(p){
    this.add=true
    this.isUpdate=true;
    
       
        this.categorieForm.patchValue(
            {
              id:p.id,
              designation:p.designation,
              description:p.description,
             }
        )
          
    

  }
  getCategories(){
    this.myServ.getResource("/categories?page="+this.page+"&&size=2&&sort=date").subscribe(
    (data)=>{
      this.categories=data;
      this.totalPages=data['page']['totalPages']
      console.log(data);
      //console.log("bbbb");
    },
    (err)=>{
      console.log(err)
      //console.log("ffff");
  
    },
  )
  }
  addCategorie(){
    let catForm=this.categorieForm.value
    console.log(catForm)
    this.myServ.postData("/addCategorie",{designation:catForm['designation'],description:catForm['description']}).subscribe(
        (data)=>{
          //console.log(data);
          this.getCategories();
          this.add=false
        },
        (err)=>{
          console.log(err);
        }
      );
  }

}
