import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { MyServiceService } from 'src/app/service/my-service.service';
import { LogInService } from 'src/app/service/log-in.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  constructor(private formB:FormBuilder,private myServ:MyServiceService,private logServ:LogInService,private route:Router) { }
  selectedFile
  currentFile
  progressBar
  upfile=false;
  url="assets/images/im.png"
  categorieForm;
  categories;
  isAuth;
  product;
  products;
  add=false;
  page=0;
  totalPages=0;
  idUpdate=false;
  idProduct
  nextPage(){
    
    if(this.page<this.totalPages){
      this.page++;
      this.getProduct();
      console.log(this.page)
    }
    
  }
  previousPage(){
    if(this.page>0){
      this.page--;
      this.getProduct();
    }
    
  }
  getPage(i){
    this.page=i;
      this.getProduct();
  }
  addP(){
    this.add=true
  }
  update(p){
    this.add=true;
    this.idUpdate=true
    this.myServ.getResource("/products/"+p.id+"/categorie").subscribe(
      
      (data)=>{
        this.url="http://localhost:3020/photo/"+p.id;
        console.log(p.id)

        this.categorieForm.patchValue(
          {
            designation:p.designation,
            description:p.designation,
            prix:p.currentPrice,
            id:p.id,
            categorie:data['id']
        }
        );
      },
      (err)=>{
        console.log(err);
      }
    );
  }
  deleProduct(){
    if(this.idProduct){
      this.myServ.deleteData("/deleteProduct/"+this.idProduct).subscribe(
        (data)=>{
          console.log(data);
          this.getProduct();
        },
        (erreur)=>{
          console.log(erreur);
        }
      );
    }
    

  }
  getProduct(){
    this.myServ.getResource("/products?page="+this.page+"&&size=2&&sort=date,desc").subscribe(
      (data)=>{
        this.products=data
        console.log(data);
        this.totalPages=data['page']['totalPages']
        
      },
      (err)=>{
        console.log(err);
      }    
  );

  }
  ConfedeleProduct(id){
    this.idProduct=id;
    console.log("yes yes yes yes")
    console.log(id)

  }
  onselectedFile(event){
    this.upfile=true
    if(event.target.files){
      this.selectedFile=event.target.files;
      var reader=new FileReader();
      reader.readAsDataURL(event.target.files[0])
      reader.onload=(event1:any)=>{
      this.url=event1.target.result;
    }
    }
    
  }
  upSelect(id){
    this.currentFile=this.selectedFile.item(0)
    
    console.log(this.currentFile)
    console.log("oooooooooooooooooooooooooooooooooooooooooooooo");
    this.myServ.uploadPhoto(this.currentFile,"/uploadPhoto/"+id).subscribe(
     
      (event)=>{
        if(event.type === HttpEventType.UploadProgress){
          this.progressBar=Math.round(100*(event.loaded/event.total))
          console.log("oooooooooooooooooooooooooooooooooooooooooooooo")
        }else if (event instanceof HttpResponse){
          alert("chargement termine")
          
        }
        //this.router.navigate(["chauffeur",this.chauffeur.idChauffeur])
        //recharger la page
        location.reload()
      },
      (err)=>{
        alert("probleme de chargement")
        console.log(err)
      }
    )
  }
  ngOnInit() {
    this.getProduct();
    this.categorieForm=this.formB.group(
      {
      designation:['',Validators.required],
      description:['',Validators.required],
      prix:['',Validators.required],
      categorie:['',Validators.required],
      id:"",
    })
    this.logServ.loadLocalStorage();
    this.isAuth=this.logServ.isAuthenticate;
    console.log(this.logServ.token)
    this.myServ.getResource("/categories").subscribe(
      (data)=>{
        this.categories=data;
        console.log(data);
        console.log("bbbb")
      },(err)=>{
        console.log(err);
      }
    )

  }
 
  get f(){return this.categorieForm.controls}
  addCategorie(){
    let catForm=this.categorieForm.value
    console.log(catForm)
    this.myServ.getResource("/categories/"+catForm['categorie']).subscribe(
      (data)=>{
        let product1={
          designation:catForm['designation'],
          id:catForm['id'],
          selected:false,
          promotion:false,
          disponiblity:true,
          currentPrice:catForm['prix'],
          categorie:data,
        }
        console.log(product1);
        this.myServ.postData("/addProduct",product1).subscribe(
          (data)=>{
            console.log(data);
            this.product=data;
           if(this.upfile){
            this.upSelect(data['id'])
           }
            
            console.log(data['id'])
            this.route.navigate(["/detailProduit/"+data['id']]);
          },
          (err)=>{
            console.log(err)
            //alert(err);
            
          }
        )
      },(err)=>{
        console.log(err);
      }
    )
    //this.myServ.postData("/addProduct",{designation:catForm['designation'],description:catForm['description']})
  }
  updateProduct(){
    let catForm=this.categorieForm.value

        let product1={
          id:catForm['id'],
          designation:catForm['designation'],
          selected:false,
          promotion:false,
          disponiblity:true,
          currentPrice:catForm['prix'],
         
        }
        console.log(product1);
        this.myServ.postData("/addProduct",product1).subscribe(
          (data)=>{
            console.log(data);
            this.product=data;
            //update photo
            if(this.upfile){
              this.upSelect(data['id'])
            }
            this.route.navigate(["/detailProduit/"+data['id']]);
          },
          (err)=>{
            console.log(err)
            alert(err);
          }
        );
      
    //this.myServ.postData("/addProduct",{designation:catForm['designation'],description:catForm['description']})
  
}

}
