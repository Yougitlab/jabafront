import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Admine1Component } from './admine1.component';

describe('Admine1Component', () => {
  let component: Admine1Component;
  let fixture: ComponentFixture<Admine1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Admine1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Admine1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
