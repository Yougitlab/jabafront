import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Admine1Component } from './admine1.component';
import { OrdersGComponent } from '../orders-g/orders-g.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { RapportComponent } from '../rapport/rapport.component';
import { AddProductComponent } from '../add-product/add-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from '../users/users.component';
import { AddCategorieComponent } from '../add-categorie/add-categorie.component';



@NgModule({
  declarations: [
    Admine1Component,
    OrdersGComponent,
    RapportComponent,
    AddProductComponent,
    UsersComponent,
    AddCategorieComponent,

  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule

  ]
})
export class Admine1Module { }
