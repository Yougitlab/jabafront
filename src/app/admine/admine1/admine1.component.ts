import { Component, OnInit, Input } from '@angular/core';
import { LogInService } from 'src/app/service/log-in.service';

@Component({
  selector: 'app-admine1',
  templateUrl: './admine1.component.html',
  styleUrls: ['./admine1.component.css']
})
export class Admine1Component implements OnInit {

  constructor(private logServ:LogInService) { }

  ngOnInit() {
    this.logServ.admine=false
  }

}
