import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {MyServiceService} from 'src/app/service/my-service.service';
import { from } from 'rxjs';
import { Product } from '../models/product-model';
import { HttpEventType, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-detail-produit',
  templateUrl: './detail-produit.component.html',
  styleUrls: ['./detail-produit.component.css']
})
export class DetailProduitComponent implements OnInit {

  constructor(private aRoute:ActivatedRoute,private servProduct:MyServiceService,private service:MyServiceService) { }

  product:Product;
  selectedFile;
  currentFile;
  progressBar;
  upSelect(event,id){
    this.selectedFile=event.target.files;
    this.currentFile=this.selectedFile.item(0)
    console.log(this.currentFile)
    this.service.uploadPhoto(this.currentFile,"/uploadPhoto/"+id).subscribe(
      (event)=>{
        if(event.type === HttpEventType.UploadProgress){
          this.progressBar=Math.round(100*(event.loaded/event.total))
          console.log(this.progressBar)
        }else if (event instanceof HttpResponse){
          alert("chargement termine")
          
        }
      }
    );
      }
  ngOnInit() {
    let p1=this.aRoute.snapshot.params['id'];
    console.log(p1)
    this.servProduct.getProduct("/products/"+p1).subscribe(
      (data)=>{
        this.product=data;
      },
      (err)=>{
        console.log(err)
        console.log(err);
      }
    )

  }

}
