import { Component, OnInit } from '@angular/core';
import { MyServiceService } from 'src/app/service/my-service.service';
import { ActivatedRoute, RouteConfigLoadEnd, Router, NavigationEnd, NavigationStart } from '@angular/router';
import { LogInService } from 'src/app/service/log-in.service';
import { CaddiesService } from 'src/app/service/caddies.service';
import { Caddy } from 'src/app/models/caddy-model';
import { HttpEventType, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {

  constructor(private service:MyServiceService,private aRoute:ActivatedRoute,private route:Router,private logServ:LogInService,private servCaddy:CaddiesService) { }
  products;
  isAuth;
  quantity=1;
  product;
  productsRecent;
  caddy:Caddy;
  selectedFile;
  currentFile;
  progressBar;
  productParTrois=[];
  getPromotion(){
    this.service.getResource("/products/search/produitPromotion").subscribe(
      (data)=>{
        this.products=data;
        console.log(data);
        
      },
      (err)=>{
        console.log(err)
        

      },
    )
  }
  addCaddy(value,id){
    this.quantity=value['quantity']
    this.service.getProduct("/products/"+id).subscribe(
      (data)=>{
        this.product=data;
        this.servCaddy.addProductToCaddy(this.product,this.quantity);
        this.caddy=this.servCaddy.currentCaddie;
        console.log(this.caddy.name);
        console.log(this.caddy.items.size);
      },
      (err)=>{
        console.log(err)
        console.log(err);
      }
    );
    
  }
  getProduct(p){
    this.service.getResource("/categories/"+p+"/products?page=1&&size=2&&sort=date").subscribe(
      (data)=>{
        this.products=data
        console.log(data);
      },
      (err)=>{
        console.log(err);
      }    
      )
  }
  getProductRecent(){
    this.service.getResource("/products?page=0&&size=8&&sort=date").subscribe(
      (data)=>{
        this.productsRecent=data
        console.log(data);
        for(let i=0;i<data['_embedded']['products'].length;i += 4){
          this.productParTrois.push({items:data['_embedded']['products'].slice(i,i+4)});
        }
        
      },
      (err)=>{
        console.log(err);
      }    
      )
  }
  upSelect(event,id){
    this.selectedFile=event.target.files;
    this.currentFile=this.selectedFile.item(0)
    console.log(this.currentFile)
    this.service.uploadPhoto(this.currentFile,"/uploadPhoto/"+id).subscribe(
      (event)=>{
        if(event.type === HttpEventType.UploadProgress){
          this.progressBar=Math.round(100*(event.loaded/event.total))
          console.log(this.progressBar)
        }else if (event instanceof HttpResponse){
          alert("chargement termine")
          
        }
        //this.router.navigate(["chauffeur",this.chauffeur.idChauffeur])
        //recharger la page
        location.reload()
      },
      (err)=>{
        alert("probleme de chargement")
        console.log(err)
      }
    )
  }
  ngOnInit() {
    this.logServ.loadLocalStorage();
    this.isAuth=this.logServ.isAuthenticate;
    let p1=this.aRoute.snapshot.params['id'];
    console.log("1111111111111111111111111111")
    console.log(p1)
    console.log(this.route.events)
    
      
    this.getProduct(p1);
    this.getProductRecent();
    
      
    
    
    
  }

}
