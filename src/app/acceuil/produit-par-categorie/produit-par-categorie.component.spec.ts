import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProduitParCategorieComponent } from './produit-par-categorie.component';

describe('ProduitParCategorieComponent', () => {
  let component: ProduitParCategorieComponent;
  let fixture: ComponentFixture<ProduitParCategorieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProduitParCategorieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProduitParCategorieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
