import { Component, OnInit } from '@angular/core';
import { CaddiesService } from 'src/app/service/caddies.service';
import { LogInService } from 'src/app/service/log-in.service';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute, NavigationStart, NavigationEnd, Router } from '@angular/router';
import { MyServiceService } from 'src/app/service/my-service.service';

@Component({
  selector: 'app-produit-par-categorie',
  templateUrl: './produit-par-categorie.component.html',
  styleUrls: ['./produit-par-categorie.component.css']
})
export class ProduitParCategorieComponent implements OnInit {

  isAuth
  products
  product
  caddy
  quantity=1
  constructor(private service:MyServiceService,private aRoute:ActivatedRoute,private route:Router,private logServ:LogInService,private servCaddy:CaddiesService) { }

  getPromotion(){
    this.service.getResource("/products/search/produitPromotion").subscribe(
      (data)=>{
        this.products=data;
        console.log(data);
        
      },
      (err)=>{
        console.log(err)
        

      },
    )
  }
  addCaddy(value,id){
    this.quantity=value['quantity']
    this.service.getProduct("/products/"+id).subscribe(
      (data)=>{
        this.product=data;
        this.servCaddy.addProductToCaddy(this.product,this.quantity);
        this.caddy=this.servCaddy.currentCaddie;
        console.log(this.caddy.name);
        console.log(this.caddy.items.size);
      },
      (err)=>{
        console.log(err)
        console.log(err);
      }
    );
    
  }
  getProduct(p){
    this.service.getResource("/categories/"+p+"/products?sort=date").subscribe(
      (data)=>{
        this.products=data
        //console.log(data);
       
      },
      (err)=>{
        console.log(err);
      }    
      )
  }
  ngOnInit() {
    console.log("1111111111111111111111111111")
    this.logServ.loadLocalStorage();
    this.isAuth=this.logServ.isAuthenticate;
    let p1=this.aRoute.snapshot.params['id'];
    this.getProduct(p1);
    console.log(p1)
    console.log(this.route.events)
    
      this.route.events.subscribe((val)=>{
       
        if(val instanceof NavigationEnd || val instanceof NavigationStart){
          console.log("jjjjjjjjjjjjjjjjjjjjjjjjj")
          let tab=val.url.split('/');
          p1=tab[tab.length-1]
          console.log(p1)
          if(p1=="Promotion"){
             console.log(p1)
            this.getPromotion()
          }else{

            this.getProduct(p1);
          }
        }
      }
      );
    }

}
