import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { OrderService } from '../service/order.service';
import { MyServiceService } from '../service/my-service.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private formB:FormBuilder,private orderServ:OrderService,private myServ:MyServiceService) { }
  userForm;
  order;
  user
  users
  ngOnInit() {
    

    this.userForm=this.formB.group(
      {
      nom:['',Validators.required],
      prenom:['',Validators.required],
      tel:['',Validators.required],
      password:['',Validators.required],
      confPassword:['',Validators.required],
    },
    {validator: this.MustMatch('password', 'confPassword')}
    )

  }
  MustMatch(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        console.log(control)
        console.log(matchingControl)

        

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}
  get f(){return this.userForm.controls}
  saveOrder(){
    
  }
  addUser(){
    let client={nom:this.userForm.value['nom'],prenom:this.userForm.value['prenom'],tel:this.userForm.value['tel'],pswd:this.userForm.value['password'],profile:"client"}
    console.log(this.userForm.value['nom']);
    this.orderServ.loadProductItem();
    this.orderServ.curentOrder.utilisateur=client;
    console.log(this.orderServ.curentOrder.products)
    console.log(this.orderServ.curentOrder.utilisateur)
    console.log(this.orderServ.curentOrder.totalOrder)
    this.myServ.postOrder("/myOrder",this.orderServ.curentOrder).subscribe(
      (data)=>{
        this.order=data;
      },
      (err)=>{
        console.log(err);
      }
    )
  }

}
