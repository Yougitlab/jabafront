import { Component, OnInit } from '@angular/core';
import { LogInService } from '../service/log-in.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private servLog:LogInService,private route:Router) { }

  private isAuthenticate;
  private currenteUser;
  private errorLog;
  ngOnInit() {
  }

  
  login(value){
    console.log(value['pseudo']);
    this.servLog.login(value['pseudo'],value['password']);
    this.isAuthenticate=this.servLog.isAuthenticate;
    
    if(this.isAuthenticate){
      this.currenteUser=this.servLog.currenteUser;
      console.log(this.currenteUser)
      this.route.navigateByUrl('');
    }else{
      this.errorLog="Login ou mot de passe incorect"
    }
    
  }
}
