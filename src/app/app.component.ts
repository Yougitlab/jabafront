import { Component, OnInit } from '@angular/core';
import { MyServiceService } from './service/my-service.service';
import { LogInService } from './service/log-in.service';
import { CaddiesService } from './service/caddies.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'saffNekhe';
  admine=true;
  categories;
  isAuth;
  sizeCaddy=0;
  constructor(private service:MyServiceService,private logServ:LogInService,private caddyServ:CaddiesService){}
  getPromotion(){
    this.service.getResource("/products/search/produitPromotion").subscribe(
      (data)=>{
        this.categories=data;
        console.log(data);
        
      },
      (err)=>{
        console.log(err)
        

      },
    )
  }
  logOut(){
    this.logServ.logOut();
  }
  ngOnInit(){
    this.sizeCaddy=this.caddyServ.getCaddy().length
    console.log(this.caddyServ.getCaddy().length)
    this.logServ.loadLocalStorage();
    this.isAuth=this.logServ.isAuthenticate;
    this.service.getResource("/categories").subscribe(
      (data)=>{
        this.categories=data;
        console.log(data);
        console.log("bbbb");
      },
      (err)=>{
        console.log(err)
        console.log("ffff");

      },
    )
  }
}
