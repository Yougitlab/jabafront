export class Product{
    public id:number;
    public designation:string;
    public selected:boolean;
    public promotion:boolean;
    public disponiblity;
    public currentPrice:number;
}