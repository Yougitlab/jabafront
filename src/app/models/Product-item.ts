import { Product } from './product-model';

export class ProductItem{
    public id:number
    public quantite:number;
    public designation:string;
    public product:Product;
    public price:number;
}